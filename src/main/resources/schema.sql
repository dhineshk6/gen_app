create table IF not EXISTS APP_USER (
   id BIGINT NOT NULL AUTO_INCREMENT,
   name VARCHAR(30) NOT NULL,
   age  INTEGER NOT NULL,
   salary REAL NOT NULL,
   PRIMARY KEY (id)
);
   
/* Populate USER Table */
INSERT INTO APP_USER(name,age,salary)
VALUES ('Sam',11,11000);
   
INSERT INTO APP_USER(name,age,salary)
VALUES ('Tom',22,22000);

INSERT INTO APP_USER(name,age,salary)
VALUES ('Tim',33,33000);
   
INSERT INTO APP_USER(name,age,salary)
VALUES ('Kim',44,44000);

commit;